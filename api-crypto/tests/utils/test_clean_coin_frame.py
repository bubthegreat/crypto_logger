"""Unit tests for crypto_logger.utils.float_or_none"""

import pandas
import pytest

from crypto_logger.utils import clean_coin_frame, COLUMNS

@pytest.fixture
def test_dataframe():
    fake_vals = [
        {
            'Circulating Supply': '$12,023,123.02',
            'Volume (24h)': 'Low Vol',
            'Price': '$1.09',
            'Symbol': 'BTC',
            '% 1h': '0.05%',
            '% 24h': '0.25%',
            '% 7d': '0.24%',
            'Market Cap': '$24.03',
        },
        {
            'Circulating Supply': '$12,023,124.02',
            'Volume (24h)': '1.0',
            'Price': '$1.08',
            'Symbol': 'BTC',
            '% 1h': '0.04%',
            '% 24h': '0.24%',
            '% 7d': '0.23%',
            'Market Cap': '$23.03',
        },
    ]
    return pandas.DataFrame(fake_vals)

@pytest.fixture
def expected_dataframe():
    fake_vals = [
        {
            'Symbol': 'BTC',
            'Market Cap': 23.03,
            'Price': 1.08,
            'Circulating Supply': 12023124.02,
            'Volume (24h)': 1.0,
            '% 1h': 0.04,
            '% 24h': 0.24,
            '% 7d': 0.23,
        },
    ]
    return pandas.DataFrame(fake_vals)[COLUMNS]


@pytest.mark.parametrize('column', COLUMNS)
def test_cleans_good(column, test_dataframe, expected_dataframe):
    """Test that values that should return zero do so correctly."""
    result = clean_coin_frame(test_dataframe)
    # Reset index so we're not using timestamp
    result.reset_index(inplace=True, drop=True)
    assert result[column].equals(expected_dataframe[column])
