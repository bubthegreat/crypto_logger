"""Unit tests for crypto_logger.utils.float_or_none"""

import pytest

from crypto_logger.utils import float_or_zero


@pytest.mark.parametrize('test_string', [
    'test',
    '$',
    '$,',
    'Not a float',
    '1/2',
    '3+4',
    '0x001'
])
def test_returns_zero(test_string):
    """Test that values that should return zero do so correctly."""
    assert float_or_zero(test_string) == 0.0

@pytest.mark.parametrize('test_string, expected', [
    ('1', 1.0),
    ('100', 100.0),
    ('1000', 1000.0),
    ('-1', -1.0),
    ('-1000', -1000.0),
    ('12345', 12345.0),
])
def test_returns_float(test_string, expected):
    """Test that values that should return None do so correctly."""
    assert float_or_zero(test_string) == expected

