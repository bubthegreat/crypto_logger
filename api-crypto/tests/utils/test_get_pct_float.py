"""Unit tests for crypto_logger.utils.get_price_float"""

import pytest

from crypto_logger.utils import get_pct_float


@pytest.mark.parametrize('test_string, expected', [
    ('100%', 100.0),
    ('50%', 50.0),
    ('0%', 0.0),
    ('1%', 1.0),
    ('1 %', 1.0),
])
def test_returns_float(test_string, expected):
    """Test that values that should return a float do so correctly."""
    result = get_pct_float(test_string)
    assert result == expected
