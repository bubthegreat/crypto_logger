"""Unit tests for crypto_logger.utils.get_price_float"""

import pytest

from crypto_logger.utils import get_price_float


@pytest.mark.parametrize('test_string, expected', [
    ('$100', 100.0),
    ('$100.00', 100.0),
    ('$100,000', 100000.0),
    ('$100,000.00', 100000.0),
    ('100', 100.0),
    ('1,000', 1000.0),
    ('1,000.00', 1000.0),
])
def test_returns_float(test_string, expected):
    """Test that values that should return a float do so correctly."""
    result = get_price_float(test_string)
    assert type(result) == float, f'Did not convert {test_string} to float'
    assert result == expected
