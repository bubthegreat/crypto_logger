"""Crypto logger data getting functions."""

import asyncio
import logging
import pandas
import re
import os
import requests

from concurrent.futures import ThreadPoolExecutor
from typing import List
from typing import Optional
from typing import Tuple
from typing import Any

from bs4 import BeautifulSoup

from crypto_logger.utils import float_or_zero
from crypto_logger.utils import get_pct_float
from crypto_logger.utils import get_price_float
from crypto_logger.utils import clean_coin_frame
from crypto_logger.utils import convert_to_graph_format_pct
from crypto_logger.utils import convert_to_graph_format_price


from crypto_logger.exceptions import GettingError
from crypto_logger.exceptions import RecordingError


logger = logging.getLogger(__name__)

POOL_EXECUTOR = ThreadPoolExecutor(max_workers=10)
DATA_PATH = 'C:\crypto_data' if os.name == 'nt' else '/crypto_data'


async def get_raw_coin_frame() -> pandas.DataFrame:
    """Get raw coin information from coinmarketcap."""
    market_url = "https://coinmarketcap.com/coins/views/all/"
    page_future = POOL_EXECUTOR.submit(requests.get, market_url)
    page = await asyncio.wrap_future(page_future)

    soup = BeautifulSoup(page.content, 'html.parser')
    market_tables = soup.find_all('table')
    table_html = str(market_tables[0])
    result = pandas.read_html(table_html)[0]
    return result


async def get_all_coins():
    """Get raw frame and clean it."""
    raw_frame = await get_raw_coin_frame()
    cleaned_frame = clean_coin_frame(raw_frame)
    return cleaned_frame


def write_coin_data(coin_data: pandas.DataFrame) -> None:
    """Write all the coin data to csv files.

    :param coin_data: dataframe with coin data from get_all_coins()
    """
    for symbol in coin_data['Symbol'].unique():
        filename = f"{symbol}.csv"
        dirname = os.path.join(DATA_PATH, symbol)
        if not os.path.exists(dirname):
            os.makedirs(dirname)
        full_path = os.path.join(dirname, filename)
        if not os.path.exists(full_path):
            coin_data[coin_data['Symbol'] == symbol].to_csv(
                full_path, header=True)
        else:
            coin_data[coin_data['Symbol'] == symbol].to_csv(
                full_path, mode='a', header=False)


async def record_coin_data():
    """Get coin data and persist it."""
    try:
        logger.info("Trying to get coin data.")
        coin_data = await get_all_coins()
    except GettingError:
        logger.error(f"Error getting the coin data: {str(err)}")
        raise

    logger.info("Got coin data - attempting to record coin data.")

    try:
        write_coin_data(coin_data)
        logger.info("Recorded coin data.")
    except RecordingError as err:
        logger.error(f"Error recording the coin data: {str(err)}")
        raise

async def _get_coin_prices_formatted(coin, format_func, start = None, end = None):
    """Get price information for a single coin."""

    end = pandas.Timestamp(end) if end else pandas.Timestamp.now()
    start_default = end - pandas.Timedelta('24h')
    start = pandas.Timestamp(start) if start else start_default

    coin_path = os.path.join(DATA_PATH, coin)
    coin_file = os.path.join(coin_path, f"{coin}.csv")
    if os.path.exists(coin_file):
        coin_file_future = POOL_EXECUTOR.submit(pandas.read_csv, coin_file)
        coin_df = await asyncio.wrap_future(coin_file_future)
        coin_df.Timestamp = pandas.to_datetime(coin_df.Timestamp)
        filtered = coin_df[(coin_df.Timestamp >= start) & (coin_df.Timestamp <= end)]
        filtered.reset_index(inplace=True)
        converted_df_future = POOL_EXECUTOR.submit(format_func, filtered)
        result = await asyncio.wrap_future(converted_df_future)
    else:
        result = None
    return result


async def _get_coin_prices_pct(coin, start, end):
    """Get price information for a single coin."""
    return await _get_coin_prices_formatted(coin, convert_to_graph_format_pct, start, end)


async def _get_coin_prices_price(coin, start, end):
    """Get price information for a single coin."""
    return await _get_coin_prices_formatted(coin, convert_to_graph_format_price, start, end)


async def get_coin_prices_pct(coins, start, end):
    """Get coin prices for multiple coins."""
    coin_data_list = [asyncio.ensure_future(_get_coin_prices_pct(coin.upper(), start=start, end=end)) for coin in coins]
    result = await asyncio.gather(*coin_data_list)
    non_empty_results = [res for res in result if res]
    return non_empty_results


async def get_coin_prices_price(coins, start, end):
    """Get coin prices for multiple coins."""
    coin_data_list = [asyncio.ensure_future(_get_coin_prices_price(coin.upper(), start=start, end=end)) for coin in coins]
    result = await asyncio.gather(*coin_data_list)
    non_empty_results = [res for res in result if res]
    return non_empty_results