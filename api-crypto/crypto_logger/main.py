"""Generic task runner for crypto logger entrypoint ont he command line."""

import argparse
import asyncio
import logging
import sys


logger = logging.getLogger(__name__)


def get_args():
    """Parse command line arguments."""
    parser = argparse.ArgumentParser(description='Get coin price.')
    parser.add_argument('coin', type=str, nargs='?', help='Get price for a single coin.')
    parser.add_argument('--coins', type=str, nargs='*', help='List prices for a list of coins.')
    parser.add_argument('--list', action="store_true", help="List available coins to choose from.")
    parser.add_argument('--market', action="store_true", help='Get market data for a coin.')
    return parser.parse_args()


def main():
    """Get coin price using get_coin_price."""
    args = get_args()
    loop = asyncio.get_event_loop()
    if args.list:
        # List available currencies
        currencies = loop.run_until_complete(get_currencies())
        for currency in currencies:
            print(f"{currency}")
    elif args.coins:
        # Show prices of coins requested
        result = loop.run_until_complete(get_prices(args.coins))
        print(result)
    elif args.coin and not args.market:
        # Show price of coin requested
        result = loop.run_until_complete(get_price(args.coin))
        print(result)
    elif args.market:
        # Show price of coin across the various markets
        if not args.coin:
            print('Must have a coin specified to get market data!')
            sys.exit(1)
        result = loop.run_until_complete(get_markets(args.coin))
        print(result.to_string(index=False))
    else:
        # Exit if they suck at giving good input.
        print("Maybe read the helpfile....?")
        sys.exit(1)

if __name__ == '__main__':
    main()