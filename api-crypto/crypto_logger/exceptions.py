"""Exception types for the crypto logger."""


class CryptoLoggerException(Exception):
    """Base crypto logger exception class."""
    pass


class GettingError(CryptoLoggerException):
    """Generic error when we have a problem getting crypto data."""
    pass


class RecordingError(CryptoLoggerException):
    """Generic error when we have a problem recording crypto data."""
    pass