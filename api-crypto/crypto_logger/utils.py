"""Collection of various utilities for the crypto logger."""

from typing import Optional
import pandas

COLUMNS = [
    'Symbol',
    'Market Cap',
    'Price',
    'Circulating Supply',
    'Volume (24h)',
    '% 1h',
    '% 24h',
    '% 7d',
]

def float_or_zero(maybe_float: str) -> Optional[float]:
    """Check if number is a float and return it or a None.
    
    :param maybe_float: String that should be a float when valid.
    :return result: Float value or None

    """
    try:
        result = float(maybe_float)
    except ValueError:
        result = 0.0
    return result


def get_pct_float(pct_str: str) -> float:
    """Remove percent sign and convert to float.
    
    :param pct_str: pct string of the format "32%"
    :return pct_float: Float percentage value * 100
    """
    no_pct = ''.join((char for char in pct_str if char not in ('%', ' ')))
    pct_float = float_or_zero(no_pct)
    return pct_float


def get_price_float(price_str: str) -> float:
    """Take a string price and convert to float.

    :param price_str: price string of format "$12,345,678.01"
    :return float_num: float of the price_string
    
    """
    no_commas = price_str.replace(',', '')
    number = no_commas.split('$')[-1]
    float_num = float_or_zero(number)
    return float_num


def clean_coin_frame(result: pandas.DataFrame) -> pandas.DataFrame:
    """Clean up raw coin information."""
    converted_df = pandas.DataFrame()

    # Convert values to floats so they're easier to use and lower
    # memory footprint.
    result['Timestamp'] = pandas.Timestamp.now()

    converted_df['Circulating Supply'] = result['Circulating Supply'].apply(
        get_price_float)
    converted_df['Price'] = result['Price'].apply(get_price_float)
    converted_df['Market Cap'] = result['Market Cap'].apply(get_price_float)
    converted_df['Volume (24h)'] = result['Volume (24h)'].apply(
        get_price_float)
    converted_df['% 1h'] = result['% 1h'].apply(get_pct_float)
    converted_df['% 24h'] = result['% 24h'].apply(get_pct_float)
    converted_df['% 7d'] = result['% 7d'].apply(get_pct_float)
    converted_df['Symbol'] = result['Symbol']
    converted_df['Timestamp'] = result['Timestamp']

    filtered_df = converted_df[
        (converted_df['Circulating Supply'] > 0) &
        (converted_df['Volume (24h)'] > 0)

    ]

    filtered_df.set_index('Timestamp', drop=True, inplace=True)
    return filtered_df[COLUMNS]


# Make this take a different value if we need percentages

def convert_to_graph_format(raw_df, value_column):
    """Convert a dataframe of coin data to the graphing format."""

    columns=['Timestamp', value_column]
    graph_df = raw_df[columns]
    graph_df.columns = ['name', 'value']
    graph_df.name = graph_df.name.apply(str)
    symbol = raw_df.Symbol[0]
    graph_data = {
        'name': symbol,
        'series': graph_df.to_dict(orient='records')
    }

    return graph_data

def convert_to_graph_format_pct(raw_df):
    """Convert a dataframe of coin data to the graphing format with percent"""

    graph_data = convert_to_graph_format(raw_df, '% 1h')
    return graph_data


def convert_to_graph_format_price(raw_df):
    """Convert a dataframe of coin data to the graphing format with price."""

    graph_data = convert_to_graph_format(raw_df, 'Price')
    return graph_data
