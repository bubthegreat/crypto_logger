"""Main app for crypto_logger API."""
from typing import List, Optional, Union, Dict

import asyncio
import logging
import os
import time
import pandas

import uvicorn
from fastapi import FastAPI

from starlette.responses import JSONResponse

from crypto_logger.getters import record_coin_data
from crypto_logger.getters import get_coin_prices_pct
from crypto_logger.getters import get_coin_prices_price
from crypto_logger.exceptions import CryptoLoggerException

from starlette.middleware.cors import CORSMiddleware

app = FastAPI()
app.add_middleware(CORSMiddleware, allow_origins=["*"], allow_methods=["*"], allow_headers=["*"], expose_headers=["*"])
logger = logging.getLogger(__name__)

WAIT_TIME = int(os.environ.get('WAIT_TIME', 600))
DATA_PATH = os.environ.get('DATA_PATH', "/crypto_data")
LOOP = asyncio.get_event_loop()


@app.on_event("startup")
async def startup():
    loop = asyncio.get_running_loop()
    loop.create_task(get_periodic_price())


@app.on_event("shutdown")
async def shutdown():
    pass


async def get_periodic_price():
    logger.info("Starting the periodic task.")
    while True:
        try:
            await record_coin_data()
        except CryptoLoggerException as err:
            logger.info(f"There was an error trying to record the coin data: {str(err)}")
        logger.info(f"Waiting {600} seconds to get more data.")
        await asyncio.sleep(600)


@app.get("/prices")
async def return_prices(coins: str, percentage: bool = False, start: pandas.Timestamp = None, end: pandas.Timestamp = None) -> JSONResponse:
    """Get market price for multiple coins.

    :param coins: comma separated list of symbols that will be fetched.
    :return result: JSON result of symbol prices.
    """
    coin_list = coins.split(',')
    if not len(coin_list) >= 1:
        raise ValueError("Please give at least one price!.")

    if percentage:
        prices = await get_coin_prices_pct(coin_list, start, end)
    else:
        prices = await get_coin_prices_price(coin_list, start, end)
    return JSONResponse(prices)


@app.get("/coins")
async def return_coins() -> list:
    """Get list of available coins.

    :return coin_list: List of coins available, sorted.

    """
    return sorted(os.listdir(DATA_PATH))


def main():
    """Run through uvicorn when run."""
    uvicorn.run("crypto_logger:app", host='0.0.0.0', port=8000, reload=True)

if __name__ == "__main__":
    main()
