"""Setup configuration and dependencies for the Crypto Logger."""

import codecs
import os
import setuptools
import re

here = os.path.abspath(os.path.dirname(__file__))


def read(*parts):
    with codecs.open(os.path.join(here, *parts), 'r') as fp:
        return fp.read()


def find_version(*file_paths):
    version_file = read(*file_paths)
    version_match = re.search(
        r"^__version__\s?=\s?['\"]([^'\"]*)['\"]",
        version_file,
        re.M,
    )
    if version_match:
        return version_match.group(1)

    raise RuntimeError("Unable to find version string.")


COMMANDS = []

PACKAGES = setuptools.find_packages(
    exclude=["*tests*", "test_*.py"]
    )

REQUIREMENTS = REQUIREMENTS = [requirement for requirement in open('requirements.txt').readlines()]

setuptools.setup(
    name='crypto_logger',
    version=find_version("crypto_logger", "__init__.py"),
    description='This is a fancy Crypto Logger!',
    packages=PACKAGES,
    python_requires='>=3.6.8',
    entry_points={'console_scripts': COMMANDS},
    install_requires=REQUIREMENTS,
)
