#!/usr/bin/env bash

docker-compose build
docker stack rm crypto_logger
docker stack deploy -c docker-compose-deploy.yml crypto_logger
