# Crypto Logger

## Application Folder Structure

### api-crypto

This folder contains the python implementation of the Crypto Logger api.  It's fully configured with python packaging for pip.  Installation should be as simple as navigating to the directory and running the command:

`pip install .`

It uses a requirements file so that you can install the requirements but not install the package (for testing purposes usually), and for good project hygiene.

### app-crypto

This folder contains the angular application for the frontend of the Crypto Logger application.  This is the "meat and potatoes" for what users will see.


## Installation

Installation is pretty straightforward - you need to have docker installed on your local machine, and you need to have docker-compose installed as well.

### docker-compose-local.yml

Since I'm lazy, everyone gets to benefit from a compose file.  This means the entire application should have a single command to get it up and running for testing.

`docker-compose up`

Once it's up, you should be able to access it via http://app-crypto.localhost if you're using the traefik container, and if you're not, http://localhost:4200

### docker-compose.yml

Also since I"m lazy, the entire deployment is dockerized into a single stack deployment command:

`docker stack deploy -f docker-compose-deploy.yml crypto-logger`

Once it's up, you should be able to access it via https://app-crypto.domain.something
