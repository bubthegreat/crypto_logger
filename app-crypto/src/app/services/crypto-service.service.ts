import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { QueryConfig } from '../interfaces/query-config';

const localHost = 'http://localhost:8000/';
const localTraefik = 'http://api.localhost/'
const remoteHost = 'https://crypto-api.bubtaylor.com/';
const currentHost = remoteHost;
const pricesURL = 'prices';
const priceQueryURL = currentHost + pricesURL
const availableCoinsURL = 'coins';




const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
  })
};


@Injectable({
  providedIn: 'root'
})
export class CryptoService {

  constructor(private http: HttpClient) { }

  getCoinsData(queryConfig: QueryConfig) {
    let params = new HttpParams();
    params = params.append('coins', queryConfig.coins)
    params = params.append('percentage', queryConfig.percentage)

    // We only want to add start/end if they exist.
    if (queryConfig.start != null) {
      params.append('start', queryConfig.start)
    }
    if (queryConfig.start != null) {
      params.append('end', queryConfig.end)
    }
    return this.http.get(priceQueryURL, { params: params })
  }

  getCoinChoices() {
    return this.http.get(currentHost + availableCoinsURL);
  }
}
