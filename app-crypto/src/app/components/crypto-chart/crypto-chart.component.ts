import { Component, OnInit, Input } from '@angular/core';
import { CryptoService } from '../../services/crypto-service.service';
import { FormControl } from '@angular/forms';
import { QueryConfig } from 'src/app/interfaces/query-config';

// Faked for the sake of what we'll use.
const fakeConfig: QueryConfig = {
  coins: "BTC,ETH,DOGE",
  percentage: "true",
  start: null,
  end: null
}


@Component({
  selector: 'app-crypto-chart',
  templateUrl: './crypto-chart.component.html',
  styleUrls: ['./crypto-chart.component.scss']
})
export class CryptoChartComponent implements OnInit {

  // Data attributes
  results;
  currentCoin;
  availableCoinsList;

  // Graph properties
  autoScale = true;
  xAxisLabel = "Time";
  yAxisLabel = "Value (USD)";
  showXAxis = true;
  showYAxis = true;
  timeline = true;

  availableCoins = new FormControl();

  constructor(private crypto: CryptoService) { }

  ngOnInit() {
    // Get the coin choices for selection.
    // this.crypto.getCoinChoices().subscribe(
      // coinChoices => {
      //   this.availableCoinsList = coinChoices;
      // });
    this.getCoinsData(fakeConfig);
  }

  getCoinsData(config: QueryConfig) {
    // Get the new coin data and set it in results.
    this.crypto.getCoinsData(config).subscribe(data => {
      this.results = data;
      console.log(data);
      this.currentCoin = true;
    });
  }
}

