import { Component, OnInit } from '@angular/core';

import { CryptoService } from '../../services/crypto-service.service';

@Component({
  selector: 'app-base-view',
  templateUrl: './base-view.component.html',
  styleUrls: ['./base-view.component.scss']
})
export class BaseViewComponent implements OnInit {

  constructor(private crypto: CryptoService) { }

  ngOnInit() {
  }
}
