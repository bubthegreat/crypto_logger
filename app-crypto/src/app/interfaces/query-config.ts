export interface QueryConfig {
    coins: string;
    percentage: string;
    start: string;
    end: string;
}

